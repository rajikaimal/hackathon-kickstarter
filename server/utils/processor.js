const regression = require('../helpers/regression')();
const totalModel = require('../models/total')();
const a = 10;
const b = 5;

//utility functions
//can be facade or factory functions
const processor = function() {
	
	function calcTotal(no1, no2, mongoose) {
		//creates a promise
		return new Promise(function(resolve, reject) {
			if(no1 === undefined || no1 === undefined) {
				reject('numbers not defined !');
			}
			//create total var from no1 and no2 supplied from routes file
			let total = no1 + no2;

			//if saving to DB call model file
			//pass total variable to saveTotal function defined in saveTotal function (file: models/total.js)
			//pass mongoose object too
			//goto model/Total.js
			//coming from Total.js :D goto routes.js
			totalModel.saveTotal(total, mongoose, function(dbResult, error) {
				if(error) {
					reject(error);
				}
				else {
					resolve(dbResult);
				}
			});
		});
	}

	function calcSubs() {
		return new Promise(function(resolve, reject) {
			if(no1 === undefined || no1 === undefined) {
				reject('numbers not defined !');
			}
			resolve(no1 + no2);
		});	
	}

	function calcRegression() {
		return regression.data;
	}

	return {
		total: calcTotal,
		subs: calcSubs,
		reg: calcRegression
	};
};

module.exports = processor;