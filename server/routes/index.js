const express = require('express');
const processor = require('../utils/processor')();

const indexRoutes = function(router, bodyParser, mongoose) {
	
	router.get('/total', function(req, res) {
		//processing
		res.json({
			total: processor.total()
		});
	})

	.post('/total', bodyParser.json(), function(req, res) {
		//retrieve no1 and no2 from POST request
		let no1 = req.body.no1;
		let no2 = req.body.no2;

		//call processor total function in utils/processor.js file
		//total returns a promise which should be accessed using then or catch(exception)
		//pass no1 and no2, and mongoose object if storing in mongoDB
		//go to utils/processor.js calcTotol method
		processor.total(no1, no2, mongoose)
			.then(function(response) {
				//after a long trip returns the response
				res.json(response);
			})
			.catch(function(err) {
				//or an error
				console.log(err);
				res.json(err);
			})
	})

	.get('/subtract', function(req, res) {
		let no1 = req.body.no1;
		let no2 = req.body.no2;


	});	

}

module.exports = indexRoutes;